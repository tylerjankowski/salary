#include <iostream>
#include <conio.h>

struct Employee {

    int id;
    std::string firstName;
    std::string lastName;
    double pay;
    double hours;

};

int main()
{
    // Length of array
    int numEmployees;

    std::cout << "How many employees do you have?\n";

    std::cin >> numEmployees;

    // Employees array
    Employee *employees = new Employee[numEmployees];

    // Get input for employees
    for (int i=0; i<numEmployees; i++) {
        std::cout << "Id of employee #" << (i + 1) << "\n";
        std::cin >> employees[i].id;

        std::cout << "First name of employee #" << (i + 1) << "\n";
        std::cin >> employees[i].firstName;

        std::cout << "Last name of employee #" << (i + 1) << "\n";
        std::cin >> employees[i].lastName;

        std::cout << "Pay for employee #" << (i + 1) << "\n";
        std::cin >> employees[i].pay;

        std::cout << "Hours per week for employee #" << (i + 1) << "\n";
        std::cin >> employees[i].hours;

        std::cout << "\n";
    }

    // begin printing employee data
    std::cout << "\n\nEmployee list:\n";

    // used to store total gross pay
    double totalGross = 0;

    // loop through every employee
    for (int i = 0; i < numEmployees; i++) {
        // Calculate weekly pay for that employee
        double tempGross = (employees[i].hours * employees[i].pay);

        std::cout << "\nEmp. #" << (i + 1) << "\n";
        std::cout << "\tId: " << employees[i].id << "\n";
        std::cout << "\tFirst Name: " << employees[i].firstName << "\n";
        std::cout << "\tLast Name: " << employees[i].lastName << "\n";
        std::cout << "\tGross pay (weekly): " << tempGross << "\n";

        // add weekly pay to total
        totalGross += tempGross;
    }

    std::cout << "\nTotal weekly pay: " << totalGross;

    _getch();
    return 0;
}